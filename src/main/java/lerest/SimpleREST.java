package lerest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleREST {

    public static void main(String[] args) {
        SpringApplication.run(SimpleREST.class, args);
    }
}
