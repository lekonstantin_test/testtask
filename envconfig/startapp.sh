#!/bin/bash
echo "STARTING"

openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -subj "/C=US/ST=Oregon/L=Oregon/O=thePoints/CN=ec2-35-166-79-126.us-west-2.compute.amazonaws.com" -keyout /etc/nginx/ssl/nginx.key  -out /etc/nginx/ssl/nginx.crt

service nginx start

java -jar /var/lerest/gs-rest-service-0.1.0.jar
echo "FINISH"
