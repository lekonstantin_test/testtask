FROM ubuntu:16.04

# Install dependencies
RUN apt-get update -y
RUN apt-get install -y default-jre nginx

# Nginx settings 
RUN mkdir /etc/nginx/ssl
COPY envconfig/nginx.conf /etc/nginx/nginx.conf
COPY envconfig/default /etc/nginx/sites-enabled/default

#App install
RUN mkdir /var/lerest
RUN rm -rf /var/lerest/*
ADD target/*.jar /var/lerest

#Start App with Nginx Proxy 
COPY envconfig/startapp.sh /var/lerest/startapp.sh
RUN ls -la /var/lerest/startapp.sh

EXPOSE 80 443

#CMD ["java","-jar","/var/lerest/gs-rest-service-0.1.0.jar"]

CMD ["/var/lerest/startapp.sh"]
